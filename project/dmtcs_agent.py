
import numpy as np
from collections import Counter
import time
from concurrent.futures import ProcessPoolExecutor, wait
import logging

from jass.agents.agent import Agent
from jass.game.rule_schieber import RuleSchieber
from jass.game.game_observation import GameObservation
from jass.game.const import next_player, NORTH, WEST, PUSH
from jass.game.game_state_util import state_from_observation
from jass.game.game_util import *

from trump_selection import calculate_trump_selection_score
from mcts_node import MonteCarloTreeSearchNode

class DMCTSAgent(Agent):
    def __init__(self):
        super().__init__()
        logging.basicConfig(level=logging.DEBUG)

        self._rule = RuleSchieber()
        self.round = 0
        self.time_budget = 8
        self.thread_count = 2
        self.thread_pool_executor = ProcessPoolExecutor(self.thread_count)
        self.threads_running = False

    def get_hands(self, obs):
        cards = np.arange(36, dtype=np.int32)
        player_hand_int = convert_one_hot_encoded_cards_to_int_encoded_list(obs.hand)
        np.random.shuffle(cards)

        for card in cards:
            if card in player_hand_int:
                cards = np.delete(cards, np.where(cards == card))
        
        hands = np.zeros([4, 36], dtype=np.int32)
        
        hands[obs.player, :] = obs.hand[:]
        
        # determine players in current trick to find out the number of cards for each player
        trick_players = []
        if obs.nr_cards_in_trick > 0:
            player = obs.trick_first_player[obs.nr_tricks]
            for _ in range(obs.nr_cards_in_trick):
                trick_players.append(player)
                player = next_player[player]
        assert len(trick_players) == obs.nr_cards_in_trick
        
        # distribute unknown cards of the current player, or one less of the player already played in the trick
        len_hand = np.count_nonzero(obs.hand)
        for p in range(NORTH, WEST + 1):
            if p != obs.player:
                # players that already played in current trick have one card less
                pn = len_hand if p not in trick_players else len_hand - 1
                hands[p] = np.zeros(36, np.int32)
                hands[p][cards[0:pn]] = 1
                cards = cards[pn:]
        return hands

    def calculate_random_hands(self, game_obs: GameObservation):
        cards = np.arange(0, 36, dtype=np.int32)
        player_hand_int = convert_one_hot_encoded_cards_to_int_encoded_list(game_obs.hand)
        np.random.shuffle(cards)
        for card in cards:
            if card in player_hand_int:
                cards = np.delete(cards, np.where(cards == card))

        hands = np.zeros(shape=[4, 36], dtype=np.int32)
        i = 0
        for x in range(0, 4):
            if x == game_obs.player_view:
                hands[x,] = game_obs.hand
            else:
                if i == 0:
                    hands[x, cards[0:9]] = 1
                if i == 1:
                    hands[x, cards[9:18]] = 1
                if i == 2:
                    hands[x, cards[18:27]] = 1
                i += 1
        return hands
        
    def action_trump(self, obs: GameObservation) -> int:
        trump_scores = {}
        player_cards = convert_one_hot_encoded_cards_to_int_encoded_list(obs.hand)
        for trump in range(0, 6):
            trump_scores[trump] = calculate_trump_selection_score(player_cards, trump)
                    
        highest_trump = max(trump_scores, key=trump_scores.get)
        highest_value = trump_scores[highest_trump]
        
        if obs.forehand == -1:
            # if forehand is not set yet, we are the forehand player
            if highest_value < 68:
                return PUSH
            
        return highest_trump
    
    def action_play_card(self, obs: GameObservation) -> int:
        start_time = time.time()
        self.round += 1
        self.round = self.round % 9
        
        best_actions = []
        valid_cards = np.flatnonzero(self._rule.get_valid_cards_from_obs(obs))
        logging.info("Valid cards: %s", valid_cards)
        #best_action = np.random.default_rng().choice(np.flatnonzero(valid_cards))
        
        future_objects = []
        if(len(valid_cards) == 1):
            best_actions.append(valid_cards[0])
        else:
            time_end = time.time() + self.time_budget
            while time.time() < time_end:
                for x in range(0, self.thread_count):
                    hands = self.calculate_random_hands(obs)
                    gameState = state_from_observation(obs, hands)
                    mcts = MonteCarloTreeSearchNode(gameState, obs.player_view)
                    future = self.thread_pool_executor.submit(mcts.best_action)
                    future_objects.append(future)
                wait(future_objects, return_when="ALL_COMPLETED")
            best_actions = list(map(lambda obj: obj.result().parent_action, future_objects))
            logging.info("Best action list: %s , Count: %s", best_actions, len(best_actions))

            """
            time_end = time.time() + 5
            while(time.time() < time_end):
                hands = self.get_hands(obs)
                gameState = state_from_observation(obs, hands)
                mcts = MonteCarloTreeSearchNode(gameState, obs.player_view)
                best_child = mcts.best_action()
                best_actions.append(best_child.parent_action)"""
            
        
        data = Counter(best_actions)
        best_action = data.most_common(1)[0][0]
        logging.info("Best action selected:  %s", best_action)
        end_time = time.time()
        logging.info("Time for play: %s", end_time - start_time)
        logging.info("----------------------------------------------------------")
        
        #valid_cards = self._rule.get_valid_cards_from_obs(obs)
        return best_action