from collections import defaultdict
import numpy as np

from jass.game.game_rule import GameRule
from jass.game.rule_schieber import RuleSchieber
from jass.game.game_state_util import observation_from_state
from jass.game.game_sim import GameSim

class MonteCarloTreeSearchNode():
    def __init__(self, gameState, player_number, parent=None, parent_action=None):
        self.gameState = gameState
        self.player_number = player_number
        self.parent = parent
        self.parent_action = parent_action
        self.children = []
        self.points = 0
        self.ruleSchieber = RuleSchieber()
        self.gameSim = GameSim(RuleSchieber())
        self._number_of_visits = 0
        self._results = defaultdict(int)
        self._results[1] = 0
        self._results[-1] = 0
        self._untried_actions = None
        self.untried_actions()
    
    def untried_actions(self):
        #valid_cards = self.ruleSchieber.get_valid_cards_from_obs(observation_from_state(self.gameState, self.player_number))
        valid_cards = self.ruleSchieber.get_valid_cards(self.gameState.hands[self.gameState.player, :], self.gameState.current_trick, self.gameState.nr_cards_in_trick, self.gameState.trump)
        self._untried_actions = np.flatnonzero(valid_cards)
        return self._untried_actions
    
    def differenceWinLoss(self):
        wins = self._results[1]
        loses = self._results[-1]
        return wins - loses
    
    def numberOfVisits(self):
        return self._number_of_visits
    
    def expand(self):
        action, self._untried_actions = self._untried_actions[-1], self._untried_actions[:-1]
        # Initilize GameSim with state
        self.gameSim.init_from_state(self.gameState)
        self.gameSim.action_play_card(action)
        child_node = MonteCarloTreeSearchNode(self.gameSim.state, self.player_number, parent=self, parent_action=action)
        self.children.append(child_node)
        return child_node
    
    def is_terminal_node(self):
        return self.gameState.nr_played_cards == 36
        
    def rollout(self):
        self.gameSim.init_from_state(self.gameState)
        
        while not self.gameSim.is_done():
            #possible_moves = self.ruleSchieber.get_valid_cards_from_obs(self.gameSim.get_observation())
            possible_moves = self.ruleSchieber.get_valid_cards(
                self.gameSim.get_observation().hand, 
                self.gameSim.get_observation().current_trick, 
                self.gameSim.get_observation().nr_cards_in_trick, 
                self.gameSim.get_observation().trump)
            action = self.rollout_policy(possible_moves)
            self.gameSim.action_play_card(action)
        
        self.points = self.gameSim.state.points
        return 1 if self.points[0] > self.points[1] else -1
    
    def backpropagate(self, result):
        self._number_of_visits += 1
        self._results[result] += 1
        if self.parent:
            self.parent.backpropagate(result)
            
    def is_fully_expanded(self):
        return len(self._untried_actions) == 0
    
    def best_child(self, c_param=0.5):
        choices_weights = [(c.differenceWinLoss() / c.numberOfVisits()) + c_param * np.sqrt((np.log(self.numberOfVisits()) / c.numberOfVisits())) for c in self.children]
        return self.children[np.argmax(choices_weights)]
    
    def rollout_policy(self, possible_moves):
        return possible_moves[np.random.randint(len(possible_moves))]
    
    def _tree_policy(self):
        current_node = self
        while not current_node.is_terminal_node():
            if not current_node.is_fully_expanded():
                return current_node.expand()
            else:
                current_node = current_node.best_child()
        return current_node
    
    def best_action(self):
        simulation_no = 100
        
        for i in range(simulation_no):
            v = self._tree_policy()
            reward = v.rollout()
            v.backpropagate(reward)
            
        return self.best_child()