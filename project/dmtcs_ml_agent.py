
import numpy as np
from collections import Counter
import time
from tensorflow import keras
import pandas as pd
from pathlib import Path

from jass.agents.agent import Agent
from jass.game.rule_schieber import RuleSchieber
from jass.game.game_observation import GameObservation
from jass.game.const import next_player, NORTH, WEST, PUSH
from jass.game.game_state_util import state_from_observation
from jass.game.game_util import *

from trump_selection import calculate_trump_selection_score
from mcts_node import MonteCarloTreeSearchNode

class DMCTSMLAgent(Agent):
    def __init__(self):
        super().__init__()
        
        self._rule = RuleSchieber()
        self.round = 0
        self.feature_columns = [
            # Diamonds
            'DA', 'DK', 'DQ', 'DJ', 'D10', 'D9', 'D8', 'D7', 'D6',
            # Hearts
            'HA', 'HK', 'HQ', 'HJ', 'H10', 'H9', 'H8', 'H7', 'H6',
            # Spades
            'SA', 'SK', 'SQ', 'SJ', 'S10', 'S9', 'S8', 'S7', 'S6',
            # Clubs
            'CA', 'CK', 'CQ', 'CJ', 'C10', 'C9', 'C8', 'C7', 'C6',
        ]
        self.model = keras.models.load_model(Path("/model/sgd_128bd-512bd-256bd-7_30_128.h5"))

    def get_hands(self, obs):
        cards = np.arange(36, dtype=np.int32)
        player_hand_int = convert_one_hot_encoded_cards_to_int_encoded_list(obs.hand)
        np.random.shuffle(cards)

        for card in cards:
            if card in player_hand_int:
                cards = np.delete(cards, np.where(cards == card))
        
        hands = np.zeros([4, 36], dtype=np.int32)
        
        hands[obs.player, :] = obs.hand[:]
        
        # determine players in current trick to find out the number of cards for each player
        trick_players = []
        if obs.nr_cards_in_trick > 0:
            player = obs.trick_first_player[obs.nr_tricks]
            for _ in range(obs.nr_cards_in_trick):
                trick_players.append(player)
                player = next_player[player]
        assert len(trick_players) == obs.nr_cards_in_trick
        
        # distribute unknown cards of the current player, or one less of the player already played in the trick
        len_hand = np.count_nonzero(obs.hand)
        for p in range(NORTH, WEST + 1):
            if p != obs.player:
                # players that already played in current trick have one card less
                pn = len_hand if p not in trick_players else len_hand - 1
                hands[p] = np.zeros(36, np.int32)
                hands[p][cards[0:pn]] = 1
                cards = cards[pn:]
        return hands

    def get_model_format_from_hand(self, hand, forehand: int):
        np.transpose(hand)
        forehand = pd.DataFrame(np.array([forehand]), columns=["FH"])
        data = pd.DataFrame([hand], columns=self.feature_columns)
        data = pd.concat([data, forehand], axis=1)
        for color in 'DHSC':
            # Jack and nine combination
            new_col = '{}_J9'.format(color)
            data[new_col] = data['{}J'.format(color)] & data['{}9'.format(color)]
        return data
        
    def action_trump(self, obs: GameObservation) -> int:
        print('Trump prediction by model')

        # calculate forehand
        forehand = int(obs.player_view == (obs.dealer + 3) % 4)
        hand = self._rule.get_valid_cards_from_obs(obs)
        data = self.get_model_format_from_hand(hand, forehand)

        # make prediction
        prediction = self.model.predict(data)[0]
        trump = prediction.argmax(axis=0)
        print("Prediction: ", prediction, " Chosen trump: ", trump)
        return int(trump)
    
    def action_play_card(self, obs: GameObservation) -> int:
        self.round += 1
        self.round = self.round % 9
        
        best_actions = []
        valid_cards = np.flatnonzero(self._rule.get_valid_cards_from_obs(obs))
        #best_action = np.random.default_rng().choice(np.flatnonzero(valid_cards))
        
        
        if(len(valid_cards) == 1):
            best_actions.append(valid_cards[0])
        else:
            time_end = time.time() + 5
            while(time.time() < time_end):
                hands = self.get_hands(obs)
                gameState = state_from_observation(obs, hands)
                mcts = MonteCarloTreeSearchNode(gameState, obs.player_view)
                best_actions.append(mcts.best_action().parent_action)
            
        data = Counter(best_actions)
        best_action = data.most_common(1)[0][0]
        
        #valid_cards = self._rule.get_valid_cards_from_obs(obs)
        return best_action