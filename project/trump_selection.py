from jass.game.const import color_of_card, offset_of_card

def calculate_trump_selection_score(cards, trump: int) -> int:
    # Score for each card of a color from Ace to 6
    # score if the color is trump
    trump_score = [15, 10, 7, 25, 6, 19, 5, 5, 5]
    # score if the color is not trump
    no_trump_score = [9, 7, 5, 2, 1, 0, 0, 0, 0]
    # score if obenabe is selected (all colors)
    obenabe_score = [14, 10, 8, 7, 5, 0, 5, 0, 0,]
    # score if uneufe is selected (all colors)
    uneufe_score = [0, 2, 1, 1, 5, 5, 7, 9, 11]
    
    result = 0
    
    for card in cards:
        color = color_of_card[card]
        offset = offset_of_card[card]
        
        if trump == 4:
            result = result + obenabe_score[offset]
            continue
        if trump == 5:
            result = result + uneufe_score[offset]
            continue
        
        if color == trump:
            val = trump_score[offset]
            result = result + val
        else:
            result = result + no_trump_score[offset]
        
    return result