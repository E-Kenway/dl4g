from dmtcs_agent import DMCTSAgent
from dmtcs_ml_agent import DMCTSMLAgent
from jass.agents.agent_random_schieber import AgentRandomSchieber
from jass.arena.arena import Arena


def play_local():
    # setup the arena
    arena = Arena(nr_games_to_play=4, print_every_x_games=1)
    player = AgentRandomSchieber()
    my_player = DMCTSAgent()
    ml_player = DMCTSMLAgent()

    arena.set_players(ml_player, player, ml_player, player)
    print('Playing {} games'.format(arena.nr_games_to_play))
    arena.play_all_games()
    for i in range(arena.nr_games_to_play):
        print("Spiel: ", i)
        print('Average Points Team 0: ', arena.points_team_0[i])
        print('Average Points Team 1: ', arena.points_team_1[i])
    
    print("-------------------------------------------------")
    print('Average Points Team 0: ', arena.points_team_0.sum())
    print('Average Points Team 1: ', arena.points_team_1.sum())

